let collection = [];

// Write the queue functions below.

function enqueue (data) {
	collection[collection.length] = data;
	return collection;
};


function dequeue() {
  collection.shift();
  return collection;
}

function print () {
	if(collection.length > 0){
		for (i = 0; i < collection.length - 1; i++){
			console.log(collection[i]);
		}
	}
	return collection;
};


function front() {
  return collection[0];
}

function size() {
  return collection.length;
}

function isEmpty() {
  if (collection.length === 0) {
    return true;
  } else {
    return false;
  }
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
